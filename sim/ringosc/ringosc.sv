`timescale 1s/1fs

module ringosc (
    input real vdd,
    output var logic out
);
real Avco = 3.4e9;
real bvco = -3.9e9;
real freq; 

initial begin out = 1;end
assign freq = Avco*vdd+bvco;

always begin
if (freq < 0.5e9)begin
		#(0.5/0.5e9) out = ~out;
		end
	else if (freq > 2e9)begin
		#(0.5/2e9) out = ~out;
		end
	else begin
		#(0.5/freq) out = ~out;
		end
end
    // YOUR IMPLEMENTATION HERE
endmodule
