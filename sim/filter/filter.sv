`timescale 1s/1fs

module filter #(
    parameter real tau=50e-9
) (
    input real in
);
    // ADD VARIABLE DECLARATIONS HERE AS NECESSARY
real t_previous = 0.0;
real y_previous = 0.0;
real x;
real y = 0;
    function real out();
		real dt;
		dt =$realtime - t_previous;
		y = $exp(-dt/tau)*y_previous + (1-$exp(-dt/tau))*x;
		t_previous = $realtime;
		y_previous = y;
		return y;
        // FILL IN FUNCTION IMPLEMENTATION
    endfunction

    always @(in) begin
		out();
		x = in;



        // UPDATE INTERNAL STATE VARIABLES AS NECESSARY
    end
endmodule
